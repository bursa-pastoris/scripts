# SPDX-FileCopyrightText: 2024 bursa-pastoris
#
# SPDX-License-Identifier: MIT

import argparse
import whoisdomain


def isavailable(domain: str) -> bool:
    '''Return if `domain` is available'''
    try:
        info = whoisdomain.query(domain).__dict__
        return False
    except whoisdomain.FailedParsingWhoisOutput:
        return True


parser = argparse.ArgumentParser(
    prog='verify-domains',
    description=('Check if given domains are available or not. They are'
                 ' deduplicated automatically.')
)
parser.add_argument('-m', '--mode',
                    choices=['available', 'registered', 'summary'],
                    default='summary', help='output format')
parser.add_argument('domainlist', nargs='+',
                    help='file(s) containing domains to check')
parser.add_argument('-s', '--sort', default=False, action='store_true',
                    help='sort the results alphabetically')
args = parser.parse_args()


tocheck =  set()
registered, available = list(), list()

for i in args.domainlist:
    with open(i, 'r') as f:
        for j in f.read().split('\n')[:-1]:
            tocheck.add(j)

for i in tocheck:
    if isavailable(i):
        available.append(i)
    else:
        registered.append(i)

if args.sort:
    available.sort()
    registered.sort()

match args.mode:
    case 'available':
        print('\n'.join(available))
    case 'registered':
        print('\n'.join(registered))
    case 'summary':
        print(f'Available domains ({len(available)}): ', ', '.join(available))
        print()
        print(f'Registered domains ({len(registered)}): ', ', '.join(registered))
