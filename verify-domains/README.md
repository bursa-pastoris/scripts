<!--
SPDX-FileCopyrightText: 2024 bursa-pastoris

SPDX-License-Identifier: CC0-1.0
-->

`verify-domain` receives a list of domain names and checks if each of them is
available or registered.

## Usage

```
usage: verify-domains [-h] [-m {available,registered,summary}] [-s]
                      domainlist [domainlist ...]

Check if given domains are available or not. They are deduplicated
automatically.

positional arguments:
  domainlist            file(s) containing domains to check

options:
  -h, --help            show this help message and exit
  -m {available,registered,summary}, --mode {available,registered,summary}
                        output format
  -s, --sort            sort the results alphabetically
```
