<!--
SPDX-FileCopyrightText: 2024 bursa-pastoris

SPDX-License-Identifier: CC0-1.0
-->

The [icon of this repository](./icon.png) is a derivative work of
[38254-new_folder-12.svg](https://commons.wikimedia.org/wiki/File:38254-new_folder-12.svg)
by [Andrew Enyart]().
