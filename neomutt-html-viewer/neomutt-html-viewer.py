#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2025 bursa-pastoris
#
# SPDX-License-Identifier: MIT

import fileinput
from bs4 import BeautifulSoup
from tempfile import NamedTemporaryFile
from subprocess import run

doc = '\n'.join(fileinput.input(encoding="utf-8"))

# Fix broken HTML
soup = BeautifulSoup(doc, 'html.parser')
soup = soup.prettify()

with NamedTemporaryFile(mode='w', prefix='neomutt-message-',
                        suffix='.html') as f:
    f.write(soup)
    print(f.name)
    run(['sudo', '-g', 'no-internet', 'surf', '-gism', f.name],
        text=True, shell=False)
