<!--
SPDX-FileCopyrightText: 2025 bursa-patoris

SPDX-License-Identifier: CC0-1.0
-->

`neomutt-html-viewer` receives an HTML document on STDIN, fixes its code, and
shows it in [`surf`](https://surf.suckless.org/).

`surf` is launched as `no-internet` group. This is meant to work with
`iptables` rules to block access to the internet for processes owned by such
group. It's expected the user is allowed to use `sudo -g no-internet` without
entering his password.
