<!--
SPDX-FileCopyrightText: 2024 bursa-pastoris

SPDX-License-Identifier: CC0-1.0
-->

This repository contains some scripts from bursa-pastoris.

Where not stated otherwise in their [REUSE](https://reuse.software) metadata,
scripts are distributed under the [Expat ("MIT") license](./LICENSES/MIT.txt).

## Script index

- [`log-public-ip`](./log-public-ip) retrieves the public IP and writes it to a
  file, accompanied by date and time.
- [`manual-duplicity-restorer`](./manual-duplicity-restorer) restores data from
  a corrupted duplicity backup without the `manifest` file, maintaining the
  original directory tree. Instructions to decrypt encrypted archives are
  included.
- [`neomutt-html-viewer`](./neomutt-html-viewer) shows HTML emails from
  neomutt.
- [`recursive-git-pull`](./recursive-git-pull) pulls all the git repositories
  in the given path and its subpaths.
- [`renew-tor-circuits`](./renew-tor-circuits) renew all circuits of tor's
  service. This script comes from [ravexina's `scripts`
  repository](https://github.com/ravexina/scripts/tree/master).
- [`sf`](./sf) clicks on SCORM presence check.
- [`texclear`](./texclear) removes build files from LaTeX. This script comes
  from [Luke Smith's `voidrice`
  repository](https://github.com/LukeSmithxyz/voidrice).
- [`tor-over-ssh-installer`](./tor-over-ssh-installer) installs an OpenSSH
  server and sets it up to be reachable through tor.
- [`verify-domains`](./verify-domains) checks if domain names in a list are
  available or already registered.
- [`yagrmm`](./yagrmm) pulls a git repository from origin and pulls somewhere
  else. Requires manual remotes configuration.
- [`yagrmm.old`](./yagrmm.old) is an old version of `yagrmm`.
