#!/bin/python3

# SPDX-FileCopyrightText: 2023, 2024 bursa-pastoris
#
# SPDX-License-Identifier: MIT

import fileinput
from email.parser import Parser
from email.message import Message
from subprocess import run, check_call
from re import sub
import sys


def clean_body(body: str) -> str:
    '''Clean the body before converting it to groff source'''

    body = body.replace('\\', '')
    body = body.strip()
    body = sub(r'(?<!\n)\n(?!\n)', '\n.br\n', body)  # Preserve line breaks
                                                     # in the output
    body = sub(r'\n{2,}', '\n\n', body)  # Remove useless line breaks

    return body


def build_groff_source(message: Message, body: str) -> str:
    '''Convert simple text message to groff source'''

    source = ''
    source += '.OH\n.EH\n'     # Set page heading to empty

    # Add metadata
    source += '.B1\n'          # Open metadata box
    for i in message.items():  # Metadata
        source += f'.B "{i[0]}:"\n'
        source += i[1].replace('\n', ' ') + '\n'
        source += '.br\n'
    source += '.B2\n'          # Close metadata box

    # Body
    source += '\n'             # Empty line betweeen metadata and body
    source += body

    return source


def build_pdf(source: bytes) -> bytes:
    '''Compile groff source to PDF'''

    output = run(
        ['groff', '-ms', '-K', 'utf8', '-T', 'pdf'],
        check=True,
        input = source,
        text=False,          # Because groff only sends the output binary
                             # document to STDOUT
        capture_output=True  # To capture the output (yes, really)
        ).stdout

    return output


if __name__ == '__main__':
    # Check if groff is available
    try:
        check_call(['groff', '--version'])
    except FileNotFoundError:
        raise SystemExit(
            "\n{red}ERROR:{end} groff is not available in your system, so I"
            " can't print your email. To install groff see"
            " {ul}https://www.gnu.org/software/groff/#downloading{end}."
            .format(red='\033[91m', ul='\033[4m', end='\033[0m')
        )

    # Parse the message
    stdin = [line for line in fileinput.input()]
    rawmessage = ''.join(stdin)
    message = Parser().parsestr(rawmessage)
    first_nonheader = stdin.index('\n')
    body = ''.join(stdin[first_nonheader:])

    body = clean_body(body)

    # Print
    source = build_groff_source(message, body)
    source = bytes(source, 'utf8')
    pdf = build_pdf(source)
    sys.stdout.buffer.write(pdf)
