<!--
SPDX-FileCopyrightText: 2023, 2024 bursa-pastoris

SPDX-License-Identifier: CC0-1.0
-->

`bp-muttprint` is bursa-pastoris' muttprint, a script to use from (neo-)mutt to
print emails to PDF.

The script requires [`groff`](https://www.gnu.org/software/groff/).

`bp-muttprint` takes the *text/plain* message from STDIN and returns its
application/pdf version to STDOUT. Therefore, to use it you must put the
following in your (neo)muttrc:

```
set print_command = "/path/to/bp-muttprint.py > /path/to/outpu.pdf"
```
