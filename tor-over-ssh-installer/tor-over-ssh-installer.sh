#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2024 bursa-pastoris
#
# SPDX-License-Identifier: MIT

# Install and configure an OpenSSH server to connect to through tor

# Check if is run as root
if [ ${EUID} -ne 0 ]; then
    echo "This script must be run as root."
    exit 1
fi

# Greet
echo "#############################"
echo "# SSH-over-tor installer    #"
echo "#############################"
echo ""
echo "This script will install an OpenSSH server and set it up to to be"
echo "reachable through tor."
echo "Make sure you are connected to a reliable internet access point and hit"
echo "<Enter> to start."
read garbage

# Install the software
echo -n "Installing the software, this may take a few minutes..."
sudo apt-get update &> /dev/null || { echo -e " \033[31mfailed\033[0m"; exit 1; }
sudo apt-get install -y openssh-server tor &> /dev/null || { echo -e " \033[31mfailed\033[0m"; exit 1; }
echo " done"

# Set up SSH
echo -n "Setting up SSH..."
SSH_CONFIG_FILE="/etc/ssh/sshd_config.d/tor_access.conf"
SSH_CONFIG="
ListenAddress 127.0.0.1
PermitRootLogin no
PasswordAuthentication yes
KbdInteractiveAuthentication no
"
echo -n "${SSH_CONFIG}" | sudo tee ${SSH_CONFIG_FILE} &> /dev/null
sudo systemctl restart ssh
echo " done"

# Set up tor
echo -n "Setting up tor..."
ONION_NAME="ssh_onion"
ONION_PATH="/var/lib/tor/${ONION_NAME}"
echo "HiddenServiceDir ${ONION_PATH}/" | sudo tee -a /etc/tor/torrc &> /dev/null
echo "HiddenServicePort 6066" 127.0.0.1:22 | sudo tee -a /etc/tor/torrc &> /dev/null
sudo systemctl restart tor
sleep 5    # Wait for tor to generate the keys before reading them
ONION_ADDRESS="$(sudo cat ${ONION_PATH}/hostname)"
echo " done"

echo ""
echo -e "\033[32mSuccess:\033[0m your device is now reachable on SSH at"
echo -e "\033[4m${ONION_ADDRESS}:6066\033[0m"
