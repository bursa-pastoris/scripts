<!--
SPDX-FileCopyrightText: 2024 bursa-pastoris

SPDX-License-Identifier: CC0-1.0
-->

`tor-over-ssh-installer` installs an OpenSSH server and sets it up to be
reachable through tor. It is designed for Debian-based systems.
