#! /bin/bash

# SPDX-FileCopyrightText: 2023 bursa-pastoris
#
# SPDX-License-Identifier: MIT

# Usage: recursive-git-pull PATH
# Pull all git repos in PATH.

repo_list="$HOME/.config/yagrmm/repolist"

while read -r repo; do
    repo="${repo/#\~/$HOME}"
    git -C "${repo}" pull
    git -C "${repo}" push
done < ${repo_list}
