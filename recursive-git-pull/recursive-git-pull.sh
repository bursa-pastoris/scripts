#! /bin/bash

# SPDX-FileCopyrightText: 2024 bursa-pastoris
#
# SPDX-License-Identifier: MIT

# Usage: recursive-git-pull PATH
# Pull all git repos in PATH.

find $1 -type d -name .git | while read repo; do
    repo=${repo%%/.git}        # Strip "/.git" trailing substring
    echo Pulling to ${repo}
    git -C "${repo}" pull
    echo Done on ${repo}
    echo
done
