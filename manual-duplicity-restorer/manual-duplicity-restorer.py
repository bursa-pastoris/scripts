#!/usr/bin/python3

# SPDX-FileCopyrightText: 2022 bursa-pastoris
#
# SPDX-License-Identifier: MIT

import shutil
import os

# FROM_SNAPSHOT
#    Path containing monovol files
#
# FROM_MULTIVOL
#    Path containing multivol files
#
# TO
#    Path to restore the files to

FROM_SNAPSHOT = "/home/user/duplicity/snapshot"
FROM_MULTIVOL = "/home/user/duplicity/multivol_snapshot"

TO = "/home/user/restore"


def main():
    # PART 1: restore monovol (`snapshot`) tree and files
    shutil.copytree(FROM_SNAPSHOT,TO)

    # PART 2: restore multivol (`multivol_snapshot`)
    for (path, dirs, files) in os.walk(FROM_MULTIVOL):
        if len(files) == 0:
            # If path is a directory, create the directory in the restore path
            if not os.path.exists(path.replace(FROM_MULTIVOL,TO,1)):
                print("Restoring path \"{}\"... ".format(path), end="")
                os.mkdir(path.replace(FROM_MULTIVOL,TO,1))
                # The triple for the directory is generated before the triples
                # for any of its subdirectories, so parents of current path
                # will always exist
                print("Done")
        if len(dirs) == 0:
            # If path is a file, recreate it the destination path
            print("Restoring file \"{}\"... ".format(path), end="")
            os.system('find "{path_to_find}" -type f -print0 | sort -zV | xargs -0 cat - > "{path_to_cat_to}"'
                    .format(path_to_find=path,
                            path_to_cat_to=path.replace(FROM_MULTIVOL,TO,1)
                           )
                     )
            print("Done")


if __name__ == '__main__':
    main()
