# SPDX-FileCopyrightText: 2024 bursa-pastoris
#
# SPDX-License-Identifier: MIT

import pyautogui
from random import randint
from time import sleep

check_sample = 'check.png'  # Image to click
p  = 0.85                   # Confidence level (0 <= p <= 1): the lower p, the
                            # higher the chance to find images slightly
                            # different from the sample)
k = 3                       # Safety inner margin on the sample

while True:
    try:
        location = pyautogui.locateOnScreen(check_sample,confidence=p)
        x = randint(location.left+k,location.left+location.width-k)
        y = randint(location.top+k,location.top+location.height-k)
        pyautogui.click(x,y)
        print('LOL I fooled the SCORM!')
    except AttributeError:
        pass
    finally:
        sleep(5)
