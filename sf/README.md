<!--
SPDX-FileCopyrightText: 2023 bursa-pastoris

SPDX-License-Identifier: CC0-1.0
-->

`sf`: a `S`CORM `f`ooler, that is, a script to prevent SCORM finding out that
you are not paying attention to a video.

`sf` checks the screen's content looking for the image SCORM uses to check
user's attention and automatically clicks a random pixel on it. To use it, take
a screenshot of such image and save it as `check.png` in `sf`'s path, then run
`sf.py`. To interrupt, use `Ctrl`+`C`.

Some configuration is available in the script itself.

You know that cheating is wrong and stuff, you assess whether using `sf` is
right or not. I use it to avoid useless mandatory pseudo-lessons that don't
teach anything anyway.
