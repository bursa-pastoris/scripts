#!/bin/sh

# SPDX-FileCopyrightText: 2019 - 2020 Luke Smith
# SPDX-FileContributor: Emre AKYÜZ
# SPDX-FileContributor: M. Yas. Davoodeh
# SPDX-FileContributor: bursa-pastoris
# SPDX-FileContributor: snailed
#
# SPDX-License-Identifier: GPL-3.0-only

# Clears the build files of a LaTeX/XeLaTeX build.
# I have vim run this file whenever I exit a .tex file.

[ "${1##*.}" = "tex" ] && {
	find "$(dirname "${1}")" -maxdepth 0 -regex '.*\(_minted.*\|.*\.\(4tc\|xref\|tmp\|pyc\|pyg\|pyo\|fls\|vrb\|fdb_latexmk\|bak\|swp\|aux\|log\|synctex\(busy\)\|lof\|lot\|maf\|idx\|mtc\|mtc0\|nav\|out\|snm\|toc\|bcf\|run\.xml\|synctex\.gz\|blg\|bbl\)\)' -delete
} || printf "Provide a .tex file.\n"

