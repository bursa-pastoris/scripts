#!/usr/bin/env python

# SPDX-FileCopyrightText: 2024 bursa-pastoris
#
# SPDX-License-Identifier: MIT

from sys import argv
import urllib.request
from bs4 import BeautifulSoup
from datetime import datetime

URL = 'https://check.torproject.org'

if len(argv) > 1:
    dest = argv[1]
else:
    dest = 'ip.txt'

with urllib.request.urlopen(URL) as r:
    html = r.read()
ip = BeautifulSoup(html, 'html.parser').find('strong').contents[0]

time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

with open(dest, 'a') as f:
    f.write(f'{time}\t{ip}\n')
