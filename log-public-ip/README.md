<!--
SPDX-FileCopyrightText: 2024 bursa-pastoris

SPDX-License-Identifier: CC0-1.0
-->

`log-public-ip` retrieves the public IP from [a Tor Project's website][check]
and writes it to a file, accompanied by date and time. The default output file
is `ip.txt` in the current directory, a different one may be passed as
positional argument (i. e. `log-public-ip.py file.txt`).



[check]: https://check.torproject.org
